import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Platform,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {loadUser, loginUser} from '../../redux/actions/userAction';
import {useDispatch, useSelector} from 'react-redux';

type Props = {
  navigation: any;
};

const LoginScreen = ({navigation}: Props) => {
  const {error, isAuthenticated} = useSelector((state: any) => state.user);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const submitHandler = (e: any) => {
    loginUser(email, password)(dispatch);
  };

  useEffect(() => {
    if (error) {
      if (Platform.OS === 'android') {
        ToastAndroid.show(
          'Email and password not matching!',
          ToastAndroid.LONG,
        );
      } else {
        Alert.alert('Email and password not matching!');
      }
    }
    if (isAuthenticated) {
      loadUser()(dispatch);
      if (Platform.OS === 'android') {
      ToastAndroid.show('Login successful!', ToastAndroid.LONG);
      } else{
        Alert.alert('Login successful!');
      }
    }
  }, [isAuthenticated, error]);

  return (
    <View className="flex-[1] items-center justify-evenly bg-black">
       <Image className='w-14 h-14' source={{uri:"https://cdn-icons-png.flaticon.com/512/1384/1384063.png"}}  />
      <View className="w-[70%]">    
        <TextInput cursorColor={"white"}
          placeholder="Enter your email"
          value={email}
          placeholderTextColor={'#818182'}
          onChangeText={text => setEmail(text)}
          className="w-full bg-gray-800  rounded-md h-[50px] border border-[#b3b0b072] px-4 my-2  text-white"
        />
        <TextInput cursorColor={"white"}
          placeholder="Enter your password"
          className="w-full bg-gray-800  rounded-md h-[50px] border border-[#b3b0b072] px-4 my-2 text-white"
          value={password}
          placeholderTextColor={'#818182'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <TouchableOpacity className="mt-6">
          <Text
            className="w-full rounded-md text-[#fff] text-center pt-[8px] text-[20px] h-[50px] bg-blue-400"
            onPress={submitHandler}>
            Login
          </Text>
        </TouchableOpacity>
        <Text
          className="pt-3 text-lime-50  text-center "
          onPress={() => navigation.navigate('Signup')}>
          Don't have any account? <Text className='text-blue-400'>Sign up</Text>
        </Text>
      </View>
    </View>
  );
};

export default LoginScreen;

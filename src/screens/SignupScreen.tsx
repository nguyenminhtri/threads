import {
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  ToastAndroid,
  Alert,
  Image,
  Platform,
} from 'react-native';
import {useEffect, useState} from 'react';
import ImagePicker, {ImageOrVideo} from 'react-native-image-crop-picker';
import {useDispatch, useSelector} from 'react-redux';
import {loadUser, registerUser} from '../../redux/actions/userAction';

type Props = {
  navigation: any;
};

const SignupScreen = ({navigation}: Props) => {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [avatar, setAvatar] = useState('');
  const dispatch = useDispatch();
  const {error, isAuthenticated} = useSelector((state: any) => state.user);

  useEffect(() => {
    if (error) {
      if(Platform.OS === 'android'){
        ToastAndroid.show(error, ToastAndroid.LONG);
      } else{
        Alert.alert(error);
      }
    }
    if (isAuthenticated) {
      loadUser()(dispatch);
    }
  }, [error, isAuthenticated]);

  const uploadImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality: 0.8,
      includeBase64: true,
    }).then((image: ImageOrVideo | null) => {
      if (image) {
        setAvatar('data:image/jpeg;base64,' + image.data);
      }
    });
  };

  const submitHandler = (e: any) => {
   if(avatar === '' || name === '' || email === ''){
    if(Platform.OS === 'android'){
    ToastAndroid.show('Please fill the all fields and upload avatar', ToastAndroid.LONG);
    } else{
      Alert.alert('Please fill the all fields and upload avatar')
    }
   } else{
    registerUser(name, email, password, avatar)(dispatch);
   }
  };

  return (
    <View className="flex-[1] items-center justify-evenly bg-black">
       <Image className='w-14 h-14' source={{uri:"https://cdn-icons-png.flaticon.com/512/1384/1384063.png"}}  />
      <View className="w-[70%]">
      
        <TextInput cursorColor={"white"}
          placeholder="Enter your name"
          value={name}
          onChangeText={text => setName(text)}
          placeholderTextColor={'#818182'}
          className="w-full  bg-gray-800  rounded-md h-[50px] border border-[#b3b0b072] px-4 my-2  text-white"
        />
        <TextInput cursorColor={"white"}
          placeholder="Enter your email"
          value={email}
          onChangeText={text => setEmail(text)}
          placeholderTextColor={'#818182'}
          className="w-full  bg-gray-800  rounded-md h-[50px] border border-[#b3b0b072] px-4 my-2  text-white"
        />
        <TextInput cursorColor={"white"}
          placeholder="Enter your password"
          className="w-full  bg-gray-800  rounded-md h-[50px] border border-[#b3b0b072] px-4 my-2  text-white"
          value={password}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
          placeholderTextColor={'#818182'}
        />
        <TouchableOpacity
          className="flex-row items-center"
          onPress={uploadImage}>
          <Image
            source={{
              uri: avatar
                ? avatar
                : 'https://cdn-icons-png.flaticon.com/512/2270/2270602.png',
            }}
            className="w-[30px] text-gray-100 h-[30px] rounded-full"
          />
          <Text className="text-blue-400 pl-2">Upload image</Text>
        </TouchableOpacity>
        <TouchableOpacity className="mt-6" onPress={submitHandler}>
          <Text className="w-full rounded-md text-[#fff] text-center pt-[8px] text-[20px] h-[50px] bg-blue-400">
            Sign Up
          </Text>
        </TouchableOpacity>
        <Text
         className="pt-3 text-lime-50  text-center "
          onPress={() => navigation.navigate('Login')}>
          Already have an account? <Text className='text-blue-400'>Sign in</Text>
        </Text>
      </View>
    </View>
  );
};

export default SignupScreen;

import {Platform} from 'react-native';

let URI = 'http://localhost:3000/api/v1';

if (Platform.OS === 'ios') {
  URI = 'https://threads-clone-ten.vercel.app/api/v1';
} else {
  URI = 'https://threads-clone-ten.vercel.app/api/v1';
}

export {URI};
